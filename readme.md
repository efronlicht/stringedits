# stringedits

The `stringedits` package contains the [`Edit` trait](https://docs.rs/stringedits/0.1.4/stringedits/trait.Edit.html) & associated iterators for simple edits of strings

- [substrings (or "splits"](https://docs.rs/stringedits/0.1.4/stringedits/trait.Edit.html#method.splits)
- [transpositions](https://docs.rs/stringedits/0.1.4/stringedits/trait.Edit.html#method.transposes)
- [deletions](https://docs.rs/stringedits/0.1.4/stringedits/trait.Edit.html#method.splits)
- [replacements](https://docs.rs/stringedits/0.1.2/stringedits/trait.Edit.html#method.replaces)
- [insertions](https://docs.rs/stringedits/0.1.4/stringedits/trait.Edit.html#method.inserts)

This is primarially a library crate for the project [spellcheck_toy](https://crates.io/crates/spellcheck_toy)

## Usage

Add stringedits = "0.1.4" under [dependencies] in your Cargo.toml, then bring the Edit trait into scope.

```rust
use stringedits::Edit;
let replaces = "ab".replaces().collect::<Vec<String>>().join(" ");
let want = concat!(
    "ab bb cb db eb fb gb hb ib jb kb lb mb nb ob pb qb rb sb tb ub vb wb xb yb zb ", // replace first character, 'a'
    "aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au av aw ax ay az", // replace second character, 'b'
);
assert_eq!(want, replaces);
```

## Author

Written by Efron Licht (efron.python@gmail.com)

## License

Available under the [MIT License](https://gitlab.com/efronlicht/stringedits/blob/master/LICENSE)
