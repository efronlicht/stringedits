use super::*;

#[test]
fn splits() {
    let got: Vec<(&str, &str)> = "foo".splits().collect();
    let want: Vec<(&str, &str)> = vec![("", "foo"), ("f", "oo"), ("fo", "o"), ("foo", "")];
    assert_eq!(want, got)
}
use std::collections::HashSet;
#[test]

fn replaces() {
    let got: Vec<String> = "ab".replaces(&ASCII_LOWERCASE).collect();
    let want: Vec<String> = vec![
        "ab", "bb", "cb", "db", "eb", "fb", "gb", "hb", "ib", "jb", "kb", "lb", "mb", "nb", "ob",
        "pb", "qb", "rb", "sb", "tb", "ub", "vb", "wb", "xb", "yb", "zb", "aa", "ab", "ac", "ad",
        "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "ao", "ap", "aq", "ar", "as",
        "at", "au", "av", "aw", "ax", "ay", "az",
    ]
    .into_iter()
    .map(ToString::to_string)
    .collect();
    assert_eq!(want, got)
}

#[test]
fn inserts() {
    let got: Vec<String> = "a".inserts(&ASCII_LOWERCASE).collect();
    let want: Vec<String> = vec![
        "aa", "ba", "ca", "da", "ea", "fa", "ga", "ha", "ia", "ja", "ka", "la", "ma", "na", "oa",
        "pa", "qa", "ra", "sa", "ta", "ua", "va", "wa", "xa", "ya", "za", "aa", "ab", "ac", "ad",
        "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "ao", "ap", "aq", "ar", "as",
        "at", "au", "av", "aw", "ax", "ay", "az",
    ]
    .into_iter()
    .map(ToString::to_string)
    .collect();
    assert_eq!(want, got)
}

#[test]
fn deletes() {
    let got: Vec<String> = "foo".deletions().collect();
    let want: Vec<String> = vec!["oo", "fo", "fo"]
        .into_iter()
        .map(ToString::to_string)
        .collect();
    assert_eq!(want, got)
}

#[test]
fn transposes() {
    let got: Vec<String> = "bar".transposes().collect();
    let want: Vec<String> = vec!["abr".to_string(), "bra".to_string()];
    assert_eq!(want, got);
}

#[test]
fn edits() {
    let got = "foo"
        .dist1edits(&ASCII_LOWERCASE)
        .collect::<HashSet<String>>();
    let want = "foo"
        .deletions()
        .chain("foo".transposes())
        .chain("foo".replaces(&ASCII_LOWERCASE))
        .chain("foo".inserts(&ASCII_LOWERCASE))
        .collect::<HashSet<String>>();
    assert_eq!(got, want);
}

#[test]
fn dist2edits() {
    let dist2: HashSet<String> = "foo".dist2edits(&ASCII_LOWERCASE).collect();
    assert!(dist2.contains("f"));
    assert!(dist2.contains("fooaa"));
    assert!(dist2.contains("of")); // delete & transpose
    assert!(dist2.contains("foof"))
}
